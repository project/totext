<?php

/**
 * @file
 * Administration page callbacks for the ToText module.
 */

/**
 * Administrative form to configure ToText.
 */
function totext_admin_form($form, &$form_state) {
  $credit_count = totext_get_credit_count();
  if ($credit_count) {
    $form['totext_count'] = array(
      '#markup' => '<p>' . format_plural($credit_count, 'You have <strong>1 credit</strong> remaining', 'You have <strong>@count credits</strong> remaining') . '</p>',
    );
  }
  $form['totext_username'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('ToText username'),
    '#description' => t('Your ToText <strong>API username</strong>, which is not the same as your administrative username.'),
    '#default_value' => variable_get('totext_username'),
  );
  $form['totext_password'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('ToText password'),
    '#description' => t('Your ToText <strong>API password</strong>, which is not the same as your administrative password.'),
    '#default_value' => variable_get('totext_password'),
  );
  $form['totext_logging'] = array(
    '#type' => 'radios',
    '#required' => TRUE,
    '#title' => t('Logging level'),
    '#options' => array(
      WATCHDOG_ERROR => t('Errors only'),
      WATCHDOG_INFO => t('Errors and successful sends'),
    ),
    '#default_value' => variable_get('totext_logging', WATCHDOG_ERROR),
  );

  return system_settings_form($form);
}

/**
 * Test ToText integration.
 */
function totext_test_form($form, &$form_state) {
  $credit_count = totext_get_credit_count();
  if ($credit_count) {
    $form['totext_count'] = array(
      '#markup' => '<p>' . format_plural($credit_count, 'You have <strong>1 credit</strong> remaining', 'You have <strong>@count credits</strong> remaining') . '</p>',
    );
  }
  $form['destination'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Destination(s)'),
    '#description' => t('A comma separated list of phone numbers to SMS.'),
  );

  $form['message'] = array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#title' => t('Message'),
    '#description' => t('The message to send.'),
  );

  $form['test_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Test mode'),
    '#description' => t("Make the API call in 'test mode' - validate etc, but don't actually send anything"),
    '#default_value' => FALSE,
  );

  $form['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show full HTTP request/response (useful for debugging)'),
    '#default_value' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send message'),
  );

  return $form;
}


/**
 * Submit callback for Test ToText integration.
 */
function totext_test_form_submit(&$form, &$form_state) {
  $message = $form_state['values']['message'];
  $destination = $form_state['values']['destination'];
  $test_mode = $form_state['values']['test_mode'];
  $debug = $form_state['values']['debug'];
  list($message_sent, $response) = totext_send_message($destination, $message, $test_mode);

  if ($message_sent) {
    drupal_set_message(t('Your message was successfully sent.'), 'status');
  }
  else {
    drupal_set_message(t('Your message failed to send. API response: @error', array('@error' => strip_tags($response->data))), 'error');
  }
  if ($debug) {
    drupal_set_message('Request:<code><pre>' . $response->request . '</pre></code>', 'warning');
    drupal_set_message('Response in (' . number_format($response->timer / 1000, 1) . ')s:<code><pre>' . _totext_format_http_response($response) . '</pre></code>', 'warning');
  }

  // Keep the submitted values.
  $form_state['rebuild'] = TRUE;
}

/**
 * Cheeky helper to format a HTTP request response object's properties.
 *
 * @param $response
 * @return string
 */
function _totext_format_http_response($response) {
  $output = '';
  $output .= $response->protocol . ' ' . $response->code . ' ' . $response->status_message . "\n";
  foreach($response->headers as $key => $value) {
    $output .= mb_convert_case($key, MB_CASE_TITLE) . ': ' . $value . "\n";
  }
  $output .= "\n" . check_plain($response->data);
  return $output;
}
